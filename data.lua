
-- Copyright (C) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local function insert_dedup(target, value)
	for i, value2 in ipairs(target) do
		if value2 == value then return false end
	end

	return table.insert(target, value)
end

if mods['BatteriesNotIncluded'] and settings.startup['k2c-batteries-not-included'].value and settings.startup['kr-rebalance-vehicles&fuels'].value then
	log('Trying to patch Batteries Not Included')

	if data.raw.locomotive['bni_electric-locomotive'] then
		data.raw.locomotive['bni_electric-locomotive'].max_speed = 0.925
		data.raw.locomotive['bni_electric-locomotive'].weight = 10000
		data.raw.locomotive['bni_electric-locomotive'].braking_force = 30
		data.raw.locomotive['bni_electric-locomotive'].burner.fuel_inventory_size = 3
		data.raw.locomotive['bni_electric-locomotive'].burner.burnt_inventory_size = 3
		data.raw.locomotive['bni_electric-locomotive'].max_power = '2MW'
		data.raw.locomotive['bni_electric-locomotive'].burner.effectivity = 1
		data.raw.locomotive['bni_electric-locomotive'].equipment_grid = 'kr-locomotive-grid'

		log('Batteries Not Included: Locomotive patched')
	end

	if data.raw.recipe['bni_electric-locomotive'] then
		data.raw.recipe['bni_electric-locomotive'].ingredients = {
			{'electric-engine-unit', 20},
			{'electronic-circuit', 10},
			{'steel-plate', 80},
			{'accumulator', 4},
		}

		log('Batteries Not Included: Locomotive recipe patched')
	end

	if data.raw.item['bni_battery-pack-empty'] then
		data.raw.item['bni_battery-pack-empty'].stack_size = 15
	end

	if data.raw.item['bni_battery-pack'] then
		data.raw.item['bni_battery-pack'].stack_size = 15
	end

	if data.raw.item['bni_battery-pack-2-empty'] then
		data.raw.item['bni_battery-pack-2-empty'].stack_size = 15
	end

	if data.raw.item['bni_battery-pack-2'] then
		data.raw.item['bni_battery-pack-2'].stack_size = 15
	end
end

if mods['Electric Furnaces'] and settings.startup['k2c-electric-furnace'].value then
	log('Trying to patch Electric Furnaces')

	local function simple_patch(index, power)
		if not data.raw.furnace[index] then return end
		local copy = table.deepcopy(data.raw.furnace[index])
		copy.energy_usage = power or '350kW'
		copy.type = 'assembling-machine'
		copy.source_inventory_size = 2

		data.raw.furnace[index] = nil
		data:extend({copy})
	end

	simple_patch('electric-stone-furnace')
	simple_patch('electric-steel-furnace')
	simple_patch('electric-furnace-2')
	simple_patch('electric-furnace-3', '560kW')
end

if mods['adamo-cinefaction'] and settings.startup['k2c-adamo-cinefaction'].value then
	log('Trying to patch Adamo Cinefaction')

	local copy = table.deepcopy(data.raw.boiler['heat-exchanger'])
	copy.name = 'k2c-heat-exchanger'
	copy.target_temperature = 165
	copy.energy_consumption = '6MW'
	copy.energy_source.max_temperature = 220
	copy.energy_source.min_working_temperature = 165
	copy.energy_source.minimum_glow_temperature = 140

	local copy_item = table.deepcopy(data.raw.item['heat-exchanger'])
	copy_item.name = 'k2c-heat-exchanger'
	copy_item.place_result = 'k2c-heat-exchanger'

	if data.raw.reactor['adamo-cinefaction-heatpump'] then
		data.raw.reactor['adamo-cinefaction-heatpump'].energy_source.emissions_per_minute = 90
	end

	data:extend({
		copy,
		copy_item,

		{
			type = 'recipe',
			name = 'k2c-heat-exchanger',
			enabled = false,

			ingredients = {
				{'boiler', 1},
				{'heat-pipe', 4},
				{'copper-plate', 15},
				{'steel-plate', 10},
			},

			result = 'k2c-heat-exchanger'
		}
	})

	table.insert(data.raw.technology['adamo-heat-processing'].effects, {
		type = 'unlock-recipe',
		recipe = 'k2c-heat-exchanger'
    })
end

if mods['adamo-carbon'] and settings.startup['k2c-adamo-carbon'].value and settings.startup['kr-finite-oil'].value then
	log('Trying to patch Adamo Carbon')

	if data.raw.resource['adamo-carbon-natural-gas'] then
		data.raw.resource['adamo-carbon-natural-gas'].infinite = false
		data.raw.resource['adamo-carbon-natural-gas'].minimum = 60000
		data.raw.resource['adamo-carbon-natural-gas'].normal = 400000
		data.raw.resource['adamo-carbon-natural-gas'].infinite_depletion_amount = 5

		data.raw.resource['adamo-carbon-natural-gas'].minable = {
			mining_time = 1,

			results = {
					{
						type = 'fluid',
						name = 'adamo-carbon-natural-gas',
						amount_min = 60,
						amount_max = 60,
						probability = 1
				}
			}
		}

		data.raw.resource['adamo-carbon-natural-gas'].autoplace = resource_autoplace.resource_autoplace_settings({
			name = 'adamo-carbon-natural-gas',
			order = 'd',
			base_density = 10,
			base_spots_per_km2 = 2.7,
			random_probability = 1 / 42,
			random_spot_size_minimum = 1,
			random_spot_size_maximum = 1,
			additional_richness = 40000,
			has_starting_area_placement = true,
			regular_rq_factor_multiplier = 1
		})
	end

	if data.raw['mining-drill']['adamo-carbon-gas-wellhead'] then
		data.raw['mining-drill']['adamo-carbon-gas-wellhead'].energy_usage = '90kW'
		data.raw['mining-drill']['adamo-carbon-gas-wellhead'].mining_speed = 2.5
	end

	if data.raw.recipe['adamo-carbon-rocket-fuel'] then
		data.raw.recipe['adamo-carbon-rocket-fuel'].category = 'fuel-refinery'
	end
end


if mods['cargo-ships'] and settings.startup['k2c-cargo-ships'].value then
	if settings.startup['kr-finite-oil'].value and data.raw.resource.deep_oil then
		data.raw.resource.deep_oil.infinite = false
		data.raw.resource.deep_oil.infinite_depletion_amount = 20
		data.raw.resource.deep_oil.minimum = 40000
		data.raw.resource.deep_oil.normal = 250000

		if data.raw.resource.deep_oil.minable and data.raw.resource.deep_oil.minable.results and #data.raw.resource.deep_oil.minable.results == 1 then
			data.raw.resource.deep_oil.minable.results[1].amount_min = 10
			data.raw.resource.deep_oil.minable.results[1].amount_max = 10
		end

	end
	data.raw.resource.deep_oil.category = 'oil'

	if settings.startup['kr-electric-poles-changes'].value then
		if data.raw['electric-pole']['floating-electric-pole'] then
			data.raw['electric-pole']['floating-electric-pole'].maximum_wire_distance = 32.25
		end
	end

	if settings.startup['kr-rebalance-vehicles&fuels'].value then
		if data.raw.locomotive.cargo_ship_engine then
			data.raw.locomotive.cargo_ship_engine.burner.fuel_category = nil
			data.raw.locomotive.cargo_ship_engine.burner.fuel_categories = {'chemical', 'vehicle-fuel'}
			data.raw.locomotive.cargo_ship_engine.max_power = (4 + (settings.startup['speed_modifier'].value - 1) * 0.9) .. 'MW'
			data.raw.locomotive.cargo_ship_engine.max_speed = 0.3 * settings.startup['speed_modifier'].value
		end

		if data.raw.locomotive.boat_engine then
			data.raw.locomotive.boat_engine.burner.fuel_category = nil
			data.raw.locomotive.boat_engine.burner.fuel_categories = {'chemical', 'vehicle-fuel'}
			data.raw.locomotive.boat_engine.max_power = (450 + (settings.startup['speed_modifier'].value - 1) * 300) .. 'kW'
			data.raw.locomotive.boat_engine.max_speed = 0.47 * settings.startup['speed_modifier'].value
		end
	end

	if data.raw.recipe.bridge_base then
		data.raw.recipe.bridge_base.ingredients = {
            {'advanced-circuit', 10},
            {'steel-plate', 35},
            {'steel-beam', 18},
            {'steel-gear-wheel', 20},
            {'rail', 10},
        }
	end

	if data.raw.recipe.chain_buoy then
		data.raw.recipe.chain_buoy.ingredients = {
            {'empty-barrel', 2},
            {'electronic-circuit', 2},
            {'steel-plate', 2},
            {'steel-beam', 2}
        }
	end

	if data.raw.recipe.buoy then
		data.raw.recipe.buoy.ingredients = {
            {'empty-barrel', 2},
            {'electronic-circuit', 2},
            {'steel-plate', 2},
            {'steel-beam', 2}
        }
	end

	if data.raw.recipe.port then
		data.raw.recipe.port.ingredients = {
            {'electronic-circuit', 5},
            {'steel-plate', 4},
            {'iron-stick', 3},
            {'steel-beam', 3}
        }
	end

	if data.raw.recipe.cargo_ship then
		data.raw.recipe.cargo_ship.ingredients = {
            {'steel-plate', 190},
            {'steel-beam', 25},
            {'engine-unit', 60},
            {'iron-gear-wheel', 20},
            {'steel-gear-wheel', 40},
            {'electronic-circuit', 30}
        }
	end

	if data.raw.recipe.oil_tanker then
		data.raw.recipe.oil_tanker.ingredients = {
            {'steel-plate', 148},
            {'steel-beam', 30},
            {'engine-unit', 60},
            {'iron-gear-wheel', 20},
            {'steel-gear-wheel', 40},
			{'electronic-circuit', 30},
			{'storage-tank', 6}
        }
	end

	if data.raw.recipe['floating-electric-pole'] then
		data.raw.recipe['floating-electric-pole'].ingredients = {
			{'empty-barrel', 4},
            {'big-electric-pole', 1},
            {'steel-plate', 3},
            {'steel-beam', 4}
        }
	end

	if data.raw.recipe.oil_rig then
		for k, value in ipairs(data.raw.recipe.oil_rig.ingredients) do
			if value[1] == 'steel-plate' then
				value[2] = 80
				break
			end
		end

		table.insert(data.raw.recipe.oil_rig.ingredients, {'steel-beam', 30})

		for k, value in ipairs(data.raw['mining-drill'].oil_rig.resource_categories) do
			if value == 'basic-fluid' then
				data.raw['mining-drill'].oil_rig.resource_categories[k] = 'oil'
				break
			end
		end
	end
end

if mods['SchallSuit'] and settings.startup['k2c-schallsuit'].value then
	-- this is a very rough fix aiming exclusively on fixing roboports missing from grids
	if data.raw['equipment-grid']['Schall-engineering-equipment-grid'] then
		insert_dedup(data.raw['equipment-grid']['Schall-engineering-equipment-grid'].equipment_categories, 'robot-interaction-equipment')
	end

	if data.raw['equipment-grid']['Schall-engineering-mk1-equipment-grid'] then
		insert_dedup(data.raw['equipment-grid']['Schall-engineering-mk1-equipment-grid'].equipment_categories, 'robot-interaction-equipment')
	end

	if data.raw['equipment-grid']['Schall-engineering-mk2-equipment-grid'] then
		insert_dedup(data.raw['equipment-grid']['Schall-engineering-mk2-equipment-grid'].equipment_categories, 'robot-interaction-equipment')
	end

	if data.raw['equipment-grid']['Schall-engineering-mk3-equipment-grid'] then
		insert_dedup(data.raw['equipment-grid']['Schall-engineering-mk3-equipment-grid'].equipment_categories, 'robot-interaction-equipment')
	end
end

if mods['SchallAlienLoot'] and mods['SchallAlienTech'] and settings.startup['k2c-schall-alien-tech'].value then
	if data.raw.tool['alien-science-pack'] then
		data.raw.tool['alien-science-pack'].icon = '__krastorio-2-compat__/graphics/item/alien-tech-card.png'

		data.raw.tool['alien-science-pack'].pictures = {
			layers = {
				{
					size = 64,
					filename = '__krastorio-2-compat__/graphics/item/alien-tech-card.png',
					scale = 0.25,
					mipmap_count = 4
				},
				{
					draw_as_light = true,
					flags = {'light'},
					size = 64,
					filename = '__Krastorio2__/graphics/icons/cards/military-tech-card-light.png',
					scale = 0.25,
					mipmap_count = 4
				}
			}
		}
	end

	data:extend({
		{
			type = 'item',
			name = 'k2c-alien-research-data',
			localised_description = {'item-description.k2c-alien-research-data'},
			icon = '__krastorio-2-compat__/graphics/item/alien-research-data.png',
			icon_size = 64,
			icon_mipmaps = 4,
			subgroup = 'science-pack',
			order = 'z[alien-science-pack-a]',
			stack_size = 200,
		}
	})

	data.raw.recipe['alien-science-pack-from-alien-ore-2'] = nil
	data.raw.recipe['alien-science-pack-from-alien-ore-3'] = nil
	data.raw.recipe['alien-science-pack-from-alien-artifact'] = nil

	data.raw.technology['alien-technology'].effects = {
		{
			type = 'unlock-recipe',
			recipe = 'k2c-alien-research-data'
		},
		{
			type = 'unlock-recipe',
			recipe = 'k2c-alien-science-pack'
		},
	}

	table.insert(data.raw.technology['alien-technology'].unit.ingredients, {'military-science-pack', 1})
	insert_dedup(data.raw.technology['alien-technology'].prerequisites, 'military-science-pack')

	data:extend({
		{
			type = 'recipe',
			name = 'k2c-alien-research-data',

			enabled = false,
			category = 'advanced-crafting',
			energy_required = 40,

			ingredients = {
				{'alien-artifact', 2},
				{'coke', 5},
				{'steel-plate', 5}
			},

			result = 'k2c-alien-research-data',
			result_count = 5,

			hide_from_player_crafting = settings.startup['alienloot-hide-machine-only-recipes'].value,
		},

		{
			type = 'recipe',
			name = 'k2c-alien-science-pack',

			enabled = false,
			category = 'advanced-crafting',
			energy_required = 20,

			ingredients = {
				{'blank-tech-card', 5},
				{'k2c-alien-research-data', 1},
				{'electronic-components', 5}
			},

			result = 'alien-science-pack',
			result_count = 5,

			hide_from_player_crafting = settings.startup['alienloot-hide-machine-only-recipes'].value,
		}
	})

	if data.raw['energy-shield-equipment']['Schall-energy-shield-mk3-equipment'] then
		insert_dedup(data.raw['energy-shield-equipment']['Schall-energy-shield-mk3-equipment'].categories, 'universal-equipment')
	end

	if data.raw['battery-equipment']['Schall-battery-mk3-equipment'] then
		insert_dedup(data.raw['battery-equipment']['Schall-battery-mk3-equipment'].categories, 'universal-equipment')
	end

	if data.raw['active-defense-equipment']['Schall-turret-equipment-particle-beam-EC'] then
		insert_dedup(data.raw['active-defense-equipment']['Schall-turret-equipment-particle-beam-EC'].categories, 'vehicle-equipment')
	end

	if data.raw['active-defense-equipment']['Schall-turret-equipment-particle-cannon-EC'] then
		insert_dedup(data.raw['active-defense-equipment']['Schall-turret-equipment-particle-cannon-EC'].categories, 'vehicle-equipment')
	end

	if data.raw['active-defense-equipment']['Schall-turret-equipment-stasis-beam-EC'] then
		insert_dedup(data.raw['active-defense-equipment']['Schall-turret-equipment-stasis-beam-EC'].categories, 'vehicle-equipment')
	end

	if data.raw['active-defense-equipment']['Schall-turret-equipment-force-cannon-EC'] then
		insert_dedup(data.raw['active-defense-equipment']['Schall-turret-equipment-force-cannon-EC'].categories, 'vehicle-equipment')
	end

	if data.raw['equipment-grid']['armor-mk3-equipment-grid'] then
		insert_dedup(data.raw['equipment-grid']['armor-mk3-equipment-grid'].equipment_categories, 'universal-equipment')
		insert_dedup(data.raw['equipment-grid']['armor-mk3-equipment-grid'].equipment_categories, 'robot-interaction-equipment')
	end

	local to_process = {
		'Schall-particle-tank-L',
		'Schall-particle-tank-M',
		'Schall-particle-tank-H',
		'Schall-particle-tank-SH',
		'Schall-particle-tank-MA',

		'Schall-ht-TR',
	}

	local _suffix = {
		'',
		'-mk1',
		'-mk2',
		'-mk3',
	}

	for i, name in ipairs(to_process) do
		for i2, suffix in ipairs(_suffix) do
			if data.raw['equipment-grid'][name .. suffix .. '-equipment-grid'] then
				insert_dedup(data.raw['equipment-grid'][name .. suffix .. '-equipment-grid'].equipment_categories, 'universal-equipment')
				insert_dedup(data.raw['equipment-grid'][name .. suffix .. '-equipment-grid'].equipment_categories, 'vehicle-equipment')
				insert_dedup(data.raw['equipment-grid'][name .. suffix .. '-equipment-grid'].equipment_categories, 'vehicle-robot-interaction-equipment')
			end
		end
	end

	if data.raw.item['Schall-satellite-bio-1'] then
		data.raw.item['Schall-satellite-bio-1'].rocket_launch_product = {'k2c-alien-research-data', 200}
	end

	if data.raw.item['Schall-satellite-bio-2'] then
		data.raw.item['Schall-satellite-bio-2'].rocket_launch_product = {'k2c-alien-research-data', 800}
	end

	if data.raw.item['Schall-satellite-astro-2'] then
		data.raw.item['Schall-satellite-astro-2'].rocket_launch_product = {'space-research-data', 4000}
	end

	if data.raw.recipe['Schall-satellite-astro-2'] then
		for i, ingredient in ipairs(data.raw.recipe['Schall-satellite-astro-2'].ingredients) do
			if ingredient[1] == 'nuclear-fuel' then
				ingredient[1] = 'rocket-fuel'
				ingredient[2] = 100
			elseif ingredient[1] == 'speed-module-3' then
				ingredient[2] = 10
			elseif ingredient[1] == 'effectivity-module-3' then
				ingredient[2] = 10
			end
		end
	end

	if data.raw.recipe['Schall-satellite-bio-2'] then
		for i, ingredient in ipairs(data.raw.recipe['Schall-satellite-bio-2'].ingredients) do
			if ingredient[1] == 'nuclear-fuel' then
				ingredient[1] = 'rocket-fuel'
				ingredient[2] = 100
			elseif ingredient[1] == 'speed-module-3' then
				ingredient[2] = 10
			elseif ingredient[1] == 'effectivity-module-3' then
				ingredient[2] = 10
			end
		end
	end
end

if mods['SchallTankPlatoon'] and settings.startup['k2c-schall-tank-platoon'].value then
	if data.raw['generator-equipment']['fusion-reactor-2-equipment'] then
		insert_dedup(data.raw['generator-equipment']['fusion-reactor-2-equipment'].categories, 'universal-equipment')
	end

	if data.raw['generator-equipment']['fusion-reactor-3-equipment'] then
		insert_dedup(data.raw['generator-equipment']['fusion-reactor-3-equipment'].categories, 'universal-equipment')
	end

	if data.raw['generator-equipment']['vehicle-nuclear-reactor-equipment'] then
		insert_dedup(data.raw['generator-equipment']['vehicle-nuclear-reactor-equipment'].categories, 'vehicle-equipment')
	end

	if data.raw['energy-shield-equipment']['vehicle-energy-shield-equipment'] then
		insert_dedup(data.raw['energy-shield-equipment']['vehicle-energy-shield-equipment'].categories, 'vehicle-equipment')
	end

	if data.raw['energy-shield-equipment']['vehicle-energy-shield-mk2-equipment'] then
		insert_dedup(data.raw['energy-shield-equipment']['vehicle-energy-shield-mk2-equipment'].categories, 'vehicle-equipment')
	end

	-- alien loot
	if data.raw['energy-shield-equipment']['Schall-vehicle-energy-shield-mk3-equipment'] then
		insert_dedup(data.raw['energy-shield-equipment']['Schall-vehicle-energy-shield-mk3-equipment'].categories, 'vehicle-equipment')
	end

	if data.raw['battery-equipment']['vehicle-battery-equipment'] then
		insert_dedup(data.raw['battery-equipment']['vehicle-battery-equipment'].categories, 'vehicle-equipment')
	end

	if data.raw['battery-equipment']['vehicle-battery-mk2-equipment'] then
		insert_dedup(data.raw['battery-equipment']['vehicle-battery-mk2-equipment'].categories, 'vehicle-equipment')
	end

	-- alien loot
	if data.raw['battery-equipment']['Schall-vehicle-battery-mk3-equipment'] then
		insert_dedup(data.raw['battery-equipment']['Schall-vehicle-battery-mk3-equipment'].categories, 'vehicle-equipment')
	end

	for i = 2, 4 do
		if data.raw['generator-equipment']['vehicle-fuel-cell-' .. i .. '-equipment'] then
			insert_dedup(data.raw['generator-equipment']['vehicle-fuel-cell-' .. i .. '-equipment'].categories, 'vehicle-equipment')
		end
	end

	local to_process = {
		'Schall-tank-L',
		'Schall-tank-M',
		'Schall-tank-H',
		'Schall-tank-SH',
		'Schall-tank-F',
		'Schall-ht-RA',
	}

	local _suffix = {
		'',
		'-mk1',
		'-mk2',
		'-mk3',
	}

	for i, name in ipairs(to_process) do
		for i2, suffix in ipairs(_suffix) do
			if data.raw['equipment-grid'][name .. suffix .. '-equipment-grid'] then
				insert_dedup(data.raw['equipment-grid'][name .. suffix .. '-equipment-grid'].equipment_categories, 'universal-equipment')
				insert_dedup(data.raw['equipment-grid'][name .. suffix .. '-equipment-grid'].equipment_categories, 'vehicle-equipment')
				insert_dedup(data.raw['equipment-grid'][name .. suffix .. '-equipment-grid'].equipment_categories, 'vehicle-robot-interaction-equipment')
			end
		end
	end
end
