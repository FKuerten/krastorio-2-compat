
-- Copyright (C) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local aircraft_values = require('includes.aircraft-values')
local aircraft_values2 = {}

for key in pairs(aircraft_values) do
	table.insert(aircraft_values2, key)
end

data:extend({
	{
		type = 'bool-setting',
		name = 'k2c-batteries-not-included',
		setting_type = 'startup',
		default_value = true
	},
	{
		type = 'bool-setting',
		name = 'k2c-electric-furnace',
		setting_type = 'startup',
		default_value = true
	},
	{
		type = 'bool-setting',
		name = 'k2c-adamo-cinefaction',
		setting_type = 'startup',
		default_value = true
	},
	{
		type = 'bool-setting',
		name = 'k2c-adamo-carbon',
		setting_type = 'startup',
		default_value = true
	},
	{
		type = 'bool-setting',
		name = 'k2c-cargo-ships',
		setting_type = 'startup',
		default_value = true
	},
	{
		type = 'bool-setting',
		name = 'k2c-aircraft',
		setting_type = 'startup',
		default_value = false
	},
	{
		type = 'string-setting',
		name = 'k2c-aircraft-gunship-grid',
		setting_type = 'startup',
		default_value = '-',
		allowed_values = aircraft_values2,
	},
	{
		type = 'string-setting',
		name = 'k2c-aircraft-jet-grid',
		setting_type = 'startup',
		default_value = '-',
		allowed_values = aircraft_values2,
	},
	{
		type = 'string-setting',
		name = 'k2c-aircraft-fortress-grid',
		setting_type = 'startup',
		default_value = '-',
		allowed_values = aircraft_values2,
	},
	{
		type = 'string-setting',
		name = 'k2c-aircraft-cargo-grid',
		setting_type = 'startup',
		default_value = '-',
		allowed_values = aircraft_values2,
	},
	{
		type = 'bool-setting',
		name = 'k2c-schallsuit',
		setting_type = 'startup',
		default_value = true
	},
	{
		type = 'bool-setting',
		name = 'k2c-schall-alien-tech',
		setting_type = 'startup',
		default_value = true
	},
	{
		type = 'bool-setting',
		name = 'k2c-schall-tank-platoon',
		setting_type = 'startup',
		default_value = true
	},
})
