
-- Copyright (C) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local aircraft_values = require('includes.aircraft-values')

if mods['Aircraft'] and settings.startup['k2c-aircraft'].value then
	local target = 'kr-gunship-grid'

	if data.raw['equipment-grid'][target] then
		local pick = aircraft_values[settings.startup['k2c-aircraft-gunship-grid'].value]

		if not pick then
			data.raw['equipment-grid'][target].width = 8
			data.raw['equipment-grid'][target].height = 4
		else
			data.raw['equipment-grid'][target].width = pick[1]
			data.raw['equipment-grid'][target].height = pick[2]
		end
	end

	target = 'kr-flying-fortress-grid'

	if data.raw['equipment-grid'][target] then
		local pick = aircraft_values[settings.startup['k2c-aircraft-fortress-grid'].value]

		if not pick then
			data.raw['equipment-grid'][target].width = 13
			data.raw['equipment-grid'][target].height = 12
		else
			data.raw['equipment-grid'][target].width = pick[1]
			data.raw['equipment-grid'][target].height = pick[2]
		end
	end

	target = 'kr-jet-grid'

	if data.raw['equipment-grid'][target] then
		local pick = aircraft_values[settings.startup['k2c-aircraft-jet-grid'].value]

		if not pick then
			data.raw['equipment-grid'][target].width = 8
			data.raw['equipment-grid'][target].height = 4
		else
			data.raw['equipment-grid'][target].width = pick[1]
			data.raw['equipment-grid'][target].height = pick[2]
		end
	end

	target = 'kr-cargo-plane-grid'

	if data.raw['equipment-grid'][target] then
		local pick = aircraft_values[settings.startup['k2c-aircraft-cargo-grid'].value]

		if not pick then
			if data.raw.car['cargo-plane'] then
				data.raw.car['cargo-plane'].equipment_grid = nil
			end
		else
			data.raw['equipment-grid'][target].width = pick[1]
			data.raw['equipment-grid'][target].height = pick[2]
		end
	end
end

-- on Krastorio 2 Launch
-- i asked nicely in Krastorio 2 Discord
-- why does it erase Alien Science Pack??/!??!?/!?/!?/r348r
-- Linver said he will look into it
-- and nothing happened after

-- this is most likely that "alien tech cards" are already present
-- in form of Military tech cards
-- since you have to collect biomass

if mods['SchallAlienTech'] and settings.startup['k2c-schall-alien-tech'].value and krastorio2_compat_SchallAlienTech_snapshot then
	for name, state in pairs(krastorio2_compat_SchallAlienTech_snapshot) do
		local tech = data.raw.technology[name]

		if tech and tech.unit and tech.unit.ingredients then
			table.insert(tech.unit.ingredients, state)
		end
	end
end
